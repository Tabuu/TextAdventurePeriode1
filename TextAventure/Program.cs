﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextAventure
{
    class Program
    {
        static string _name;
        static List<string> _inventory = new List<string>();

        static int _result = 999;
        static void Main(string[] args)
        {
            ResetGame();
            bool _running = true;
            while (_running)
            {
                Layout();
                PlayMusic("s_main_menu.wav", true);
                Menu(0);
            }
        }

        public static void ResetGame()
        {
            _name = "Jan Klaasen";
            _inventory.Clear();
            _result = 999;

        }

        public static void PrintFromFile(string fileName)
        {
            string[] text = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\text\\" + fileName);
            foreach (string line in text)
            {
                Console.WriteLine(line);
            }
        }

        public static void Title()
        {
            Console.Clear();
            PrintFromFile("layout.txt");
            string titel = "Text Adventure: A Shard of Time BETA";
            SetPosition((89 - titel.Length) / 2, 1);

            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(titel);
            Console.ResetColor();
            Console.Title = titel;
        }

        //MAKING START MENU
        public static void Menu(int option)
        {

            Title();

            string[] opties = { "Start Game", "Opties", "Tutorial", "Credits" };

            for (int i = 0; i < opties.Length; i++)
            {
                SetPosition(2, 24 + i - 1);
                if (i == option)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(">" + opties[i]);
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine(" " + opties[i]);
                }
            }

            ConsoleKey input = GetKey();
            if (input == ConsoleKey.DownArrow && option != opties.Length - 1)
            {
                Menu(option + 1);
            }
            else if (input == ConsoleKey.UpArrow && option > 0)
            {
                Menu(option - 1);
            }
            else if (input == ConsoleKey.Enter)
            {
                MenuSelect(option);
            }
            else
            {
                Menu(option);
            }
        }


        //MENU SELECT
        public static void MenuSelect(int selection)
        {
            switch (selection)
            {
                case 0:
                    One();
                    break;
                default:
                    EmptyOption();
                    break;


            }
        }

        public static String[] handleTextScreen(string[] text)
        {
            for (int i = 0; i < 21; i++)
            {

                SetPosition(1, 1 + i);

                try
                {
                    for (int i2 = 0; i2 < text[i].Length; i2++)
                    {
                        Console.Write(text[i][i2]);
                    }

                    for (int i2 = 0; i2 < 87 - text[i].Length; i2++)
                    {
                        Console.Write(" ");
                    }
                }
                catch { Console.Write("                                                                                      "); }
            }

            return text;
        }

        public static int handleOptions(string[] options, int selected)
        {
            inventoryHandler();
            for (int i = 0; i < 5; i++)
            {
                SetPosition(2, 23 + i);
                Console.Write("                                                                                      ");
            }
            for (int i = 0; i < options.Length; i++)
            {
                if (i < 5)
                {
                    SetPosition(2, 24 + i - 1);
                }
                else if (i >= 20)
                {
                    SetPosition(82, 24 + (i - 20) - 1);
                }
                else if (i >= 15)
                {
                    SetPosition(62, 24 + (i - 15) - 1);
                }
                else if (i >= 10)
                {
                    SetPosition(42, 24 + (i - 10) - 1);
                }
                else if (i >= 5)
                {
                    SetPosition(22, 24 + (i - 5) - 1);
                }
                if (i == selected)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.White;
                    if (i >= 10)
                    {
                        Console.WriteLine(">   " + options[i]);
                    }
                    else
                    {
                        Console.WriteLine(">  " + options[i]);
                    }

                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine(i + ". " + options[i]);
                }
            }

            ConsoleKey input = GetKey();
            SetPosition(119, Console.WindowHeight - 1);
            if (input == ConsoleKey.Enter)
            {
                _result = selected;
            }
            else if (input == ConsoleKey.DownArrow && selected != (options.Length - 1))
            {
                handleOptions(options, selected + 1);
            }
            else if (input == ConsoleKey.UpArrow && selected != 0)
            {
                handleOptions(options, selected - 1);
            }
            else if (input != ConsoleKey.DownArrow || input != ConsoleKey.UpArrow || input != ConsoleKey.LeftArrow || input != ConsoleKey.RightArrow || input != ConsoleKey.Enter)
            {
                handleOptions(options, selected);
            }
            return _result;

        }
        public static void inventoryHandler()
        {
            int extraItems = 0;
            for (int i = 0; i < 27; i++)
            {
                SetPosition(89, 1 + i);
                Console.Write("                            ");
            }
            for (int i = 0; i < _inventory.Count; i++)
            {
                if (i > 26)
                {
                    extraItems++;
                    SetPosition(89, 27);
                    Console.WriteLine("   And " + extraItems + " more...");
                }
                else
                {
                    SetPosition(89, 1 + i);
                    Console.Write(_inventory[i]);
                }

            }
        }


        public static void InventoryAdd(string itemName)
        {
            _inventory.Add(itemName);
        }

        public static void InventoryRemove(int ID)
        {
            _inventory.RemoveAt(ID);
        }

        public static string handleTextAnswer()
        {
            SetPosition(2, 23);
            Console.Write("                                                                                ");
            SetPosition(2, 24);
            Console.Write("                                                                                ");
            SetPosition(2, 25);
            Console.Write("                                                                                ");
            SetPosition(2, 26);
            Console.Write("                                                                                ");
            SetPosition(2, 27);
            Console.Write("                                                                                ");
            SetPosition(2, 26);
            Console.Write("> ");
            string answer = Console.ReadLine();
            return answer;
        }

        public static void Layout()
        {
            SetPosition(0, 0);
            PrintFromFile("layout.txt");
            SetPosition(119, Console.WindowHeight - 1);
        }

        //START STORY
        public static void One()
        {
            PlayMusic(false);
            string[] text = {
                "Hi,",
                "What is your name?"
                            };

            string[] options = { "Say", "Ignore" };
            Clear();
            Layout();
            handleTextScreen(text);

            if (handleOptions(options, 0) == 0)
            {
                SetPosition(2, 25);
                _name = handleTextAnswer().ToUpper();
                if (_name.Length > 10)
                {
                    One();
                }
                handleTextScreen(new string[] { "Hi " + _name + ",", "Let's get started!" });
                WaitForKey();
                Two();
            }
            else
            {
                handleTextScreen(new string[] { "Oh, so you're just ignoring me then..." });
                WaitForKey();
                handleTextScreen(new string[] { "Nope... it's over... don't even try..." });
                WaitForKey();
                handleTextScreen(new string[] { "I'm a very sensitive game you know." });
                WaitForKey();
                handleTextScreen(new string[] { "..." });
                WaitForKey();
                handleTextScreen(new string[] { "Oh you know I can't stay mad at you." });
                WaitForKey();
                One();
            }
        }

        static bool _clock = true;

        public static void Two()
        {
            handleTextScreen(new string[] { "Mom: " + _name + " could you give me that CLOCK?" });
            int keuze = handleOptions(new string[] { "Say", "Look around", "Look at", "Give item" }, 0);
            if (keuze == 0)
            {
                SetPosition(2, 25);
                handleTextScreen(new string[] { "Mom: What do you mean with \"" + handleTextAnswer().ToUpper() + "\" just give me the CLOCK please" });
                WaitForKey();
                Two();
            }
            else if (keuze == 1)
            {
                handleTextScreen(new string[] { "This room is quite clean... ",
                    "You see some PICTURES, an old PAINTING, an antique CLOCK and some furniture." });

                WaitForKey();
                Two();
            }
            else if (keuze == 2)
            {
                handleTextScreen(new string[] { "What is the item you want to look at?" });
                string lookat = handleTextAnswer();
                if (lookat.ToUpper() == "PAINTING")
                {
                    handleTextScreen(new string[] { "* You're looking at a painting of a heroic man on a hill.", "* The sky in the painting seems to be purple." });
                }
                else if (lookat.ToUpper() == "PICTURES")
                {
                    handleTextScreen(new string[] { "* It seems to be a family picture of your family.", "* The picture includes you, your mom, your father and your sister",
                    "* In the picture you look quite a bit younger... probably a picture of 3 years ago",
                    "* The setting looks like some amusement park"});
                }
                else if (lookat.ToUpper() == "CLOCK")
                {
                    if (_clock)
                    {
                        handleTextScreen(new string[] { "* That's just an old clock, doesn't seem very special", "*It seems like there is a little purple stone stuck between two gears","* It doesn't tick anymore, maybe it's broken.","", "Wanna pick it up?" });

                        int clockKeuze = handleOptions(new string[] { "Yes", "No" }, 0);

                        if (clockKeuze == 0)
                        {
                            _inventory.Add("Clock");
                            _clock = false;
                        }
                    } else
                    {
                        handleTextScreen(new string[] { "There is no clock anymore." });
                        WaitForKey();
                    }


                }
                else
                {
                    handleTextScreen(new string[] { "No such item can be spoted in the room." });
                }
                WaitForKey();
                Two();
            } else if (keuze == 3)
            {
                if (_inventory.Count == 0)
                {
                    handleTextScreen(new string[] { "* Your inventory seems to be empty" });
                    WaitForKey();
                    Two();
                }
                else
                {
                    handleTextScreen(new string[] { "What item do you want to give" });
                    string give = handleTextAnswer();
                    if (CheckForItem(give, 1))
                    {
                        handleTextScreen(new string[] {"Thanks " + _name+"!"});
                        InventoryRemove("Clock", 1);
                        inventoryHandler();
                        WaitForKey();
                        handleTextScreen(new string[] { "Mom: OH NO!", "*Sound of breaking glass and wood*" });
                        WaitForKey();
                        Three();
                    }
                    else
                    {
                        handleTextScreen(new string[] { "That's not what I ment... Give me the clock please."});
                        WaitForKey();
                        Two();
                    }
                }
            }
        }

        public static void EmptyOption()
        {
            handleTextScreen(new string[] { "This doesn't seem to do anything..." });
            WaitForKey();
        }

        public static void InventoryRemove(string name, int amount)
        {
            int removed = 0;
            for (int i = 0; i < _inventory.Count; i++)
            {
                if (removed == amount)
                {

                }
                else if (_inventory[i] == name)
                {
                    _inventory.RemoveAt(i);
                    removed++;
                }
            }
        }

        public static void Three()
        {
            handleTextScreen(new string[] { "All of a suden you're standing in some place that looks very weird.", "", "What do you do?" });
            int keuze = handleOptions(new string[] { "Shout", "Walk", "Scan the area" }, 0);
            if (keuze == 0)
            {
                handleTextScreen(new string[] { "What do you shout?" });
                string shout = handleTextAnswer();
                handleTextScreen(new string[] { _name+": " + shout.ToUpper() + "!!!", "" });
                WaitForKey();
                handleTextScreen(new string[] {"You hear your own echo.", "("+ shout+", "+ shout + ", " + shout +")" });
                WaitForKey();
                Three();
            }
            else if (keuze == 1)
            {
                handleTextScreen(new string[] { "In what direction do you walk?" });
                string direction = handleTextAnswer();
                if (direction.ToUpper() == "WEST")
                {
                    Four();
                }
                else
                {
                    handleTextScreen(new string[] { "ather 5km you're still standing in the middle of nowhere..." });
                    int keuze2 = handleOptions(new string[] { "Go back", "Scan the area" }, 0);
                    if (keuze2 == 0)
                    {
                        Three();
                    }else
                    {
                        handleTextScreen(new string[] { "*You're standing on old and broken tiles.", "*Grass and small bushes are growing trough the gaps of the tiles", "*Around you is emptyness", "*The sky seems purple"});
                        WaitForKey();
                        Three();
                    }

                }
            }else if (keuze == 2)
            {
                handleTextScreen(new string[] { "*You're standing on old and broken tiles.", "*Grass and small bushes are growing trough the gaps of the tiles", "*Around you is emptyness", "*The sky seems purple" });
                WaitForKey();
                Three();
            }
        }

        public static void Four()
        {
            PlayMusic("epic.wav", true);
            handleTextScreen(new string[] { "This is the end of the beta ;(",
                "The programming team did not have any more time to finish this project just yet",
            "", "I hope you got a good look at the mechanics of the game and I hope you will play the full release once it is finished"});
            WaitForKey();
        }

        //END STORY

        public static bool CheckForItem(string item, int amount)
        {
            int inInv = 0;
            for (int i = 0; i < _inventory.Count; i++)
            {
                if (_inventory[i].ToUpper() == item.ToUpper())
                {
                    inInv++;
                }
            }
            if (inInv == amount)
            {
                return true;
            }
            return false;
        }

        public static void WaitForKey()
        {
            SetPosition(119, Console.WindowHeight - 2);
            Console.ReadKey();
            SetPosition(119, Console.WindowHeight - 1);
        }

        //PRINTS STRING ARRAYS (SUPER HANDY FOR BIG AMOUNTS OF TEXT)
        public static void PrintText(String[] text, int delay)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Console.WriteLine(text[i]);
                Wait(delay);
            }
        }

        //JUST PRINT
        public static void Print(string text)
        {
            Console.WriteLine(text);
        }


        //PLAYS A WAV MUSIC FILE FROM THE "MUSIC" DIR
        public static void PlayMusic(string wavFile, bool loop)
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "\\music\\" + wavFile;

            if (loop)
            {
                player.Play();
            }
            else if (!loop)
            {
                player.PlayLooping();
            }

        }

        public static void PlayMusic(bool playMusic)
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            if (!playMusic)
            {
                player.Stop();
                player.Dispose();
            }

        }

        //SETCURSOR POSTITION
        public static void SetPosition(int x, int y)
        {
            Console.SetCursorPosition(x, y);
        }


        //WAITS FOR KEY IMPUT AND RETURNS IT AS A CONSOLEKEY
        public static ConsoleKey GetKey()
        {
            return Console.ReadKey(true).Key;
        }


        //PRINTS 'X' AMOUNT OF EMPTY LINES
        public static void Lines(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                Console.WriteLine("");
            }
        }

        //PRINTS TEXT CHAR FOR CHAR WITH 'X' DELAY BETWEEN THEM
        public static void TypeMachine(string text, int delay)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Console.Write(text[i]);
                Thread.Sleep(delay);
            }
            Console.Write("\n");
        }

        //CLEAR CONSOLE
        public static void Clear()
        {
            Console.Clear();
        }

        //WAIT FOR 'X' AMOUNT OF TIME
        public static void Wait(int time)
        {
            Thread.Sleep(time);
        }

    }
}

